from fastapi import APIRouter, Depends

from src.infrastructure.routes import AuthRoute
from src.domain.users.schemas import (
    requests as request_schemas,
    responses as response_schemas,
    BaseResponse
)
from src.domain.users.services import UsersServices
from src.infrastructure.dependencies import get_request_user


users_router = APIRouter(prefix='/users/v1')
auth_users_router = APIRouter(prefix='/users/v1', route_class=AuthRoute)
use_cases = UsersServices()


@users_router.patch('', response_model=response_schemas.UpdateUserResponse)
async def update_user(
        data: request_schemas.UpdateUserRequest,
        user=Depends(get_request_user)
):
    return await use_cases.update_user(user.id, data)


@users_router.post('/registration', response_model=BaseResponse)
async def registration(
        user_info: request_schemas.RegistrationRequest
):
    return await use_cases.register_user(user_info)


@users_router.post('/login', response_model=response_schemas.LoginResponse)
async def login(
        login_info: request_schemas.LoginRequest
):
    return await use_cases.login_user(login_info)


@users_router.post('/refresh', response_model=response_schemas.RefreshResponse)
async def refresh(
        refresh_info: request_schemas.RefreshRequest
):
    return await use_cases.refresh_tokens(refresh_info)


@auth_users_router.post('/ws_token', response_model=response_schemas.WsTokenResponse)
async def update_user(
        user=Depends(get_request_user)
):
    return await use_cases.create_ws_token(user.id)
