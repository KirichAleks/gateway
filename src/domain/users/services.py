from src.domain.users.schemas.requests import UpdateUserRequest, RefreshRequest, LoginRequest, RegistrationRequest
from src.infrastructure.http_client import HttpClient
from src.domain.enums import Services


class UsersServices:
    client = HttpClient

    async def register_user(self, data: RegistrationRequest):
        response = await self.client.send_request(
            'post',
            '/users/v1/registration',
            data=data.dict(),
            service=Services.USERS
        )
        return response

    async def login_user(self, data: LoginRequest):
        response = await self.client.send_request(
            'post',
            '/users/v1/login',
            data=data.dict(),
            service=Services.USERS
        )
        return response

    async def refresh_tokens(self, data: RefreshRequest):
        response = await self.client.send_request(
            'post',
            '/users/v1/refresh',
            data=data.dict(),
            service=Services.USERS
        )
        return response

    async def update_user(self, user_id: str, data: UpdateUserRequest):
        response = await self.client.send_request(
            'patch',
            'users/v1',
            data=data.dict(),
            service=Services.USERS,
            request_user=user_id
        )
        return response

    async def create_ws_token(self, user_id: str):
        response = await self.client.send_request(
            'post',
            '/users/v1/ws_token',
            service=Services.USERS,
            request_user=user_id
        )
        return response
