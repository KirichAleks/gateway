from src.domain.common.use_case import UseCase
from src.infrastructure.http_client import HttpClient

from fastapi import WebSocket

from src.infrastructure.ws.connections_manager import ConnectionManager


class ChatUseCases(UseCase):
    client = HttpClient
    connections_manager = ConnectionManager()

    def __int__(
            self,
            websocket: WebSocket,
            input_data: dict | None,
            user_id: str | None,
            *argc, **kwargs
    ):
        super().__init__(input_data, user_id)
        self.websocket = websocket
