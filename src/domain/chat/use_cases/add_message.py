from src.domain.chat.schemas.requests import AddMessageRequest
from src.domain.chat.use_cases.base import ChatUseCases


class AddMessage(ChatUseCases):
    request_schema = AddMessageRequest
    validated_request: request_schema

    async def _run(self):
        response = await self.client.send_request(
            'post',
            f'/chat/v1/{self.validated_request.chat_id}/messages',
            data=self.validated_request.message_data.dict(),
            request_user=self.user_id
        )
        return response
