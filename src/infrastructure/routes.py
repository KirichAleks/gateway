import traceback
from typing import Callable

from fastapi import Request, Response
from fastapi.routing import APIRoute
from starlette.responses import JSONResponse

from src.infrastructure.auth_service import decode_token
from src.infrastructure.exceptions import NotAuthorized
from src.settings import get_settings


settings = get_settings()


class AuthRoute(APIRoute):
    def get_route_handler(self) -> Callable:
        original_route_handler = super().get_route_handler()

        async def custom_route_handler(request: Request) -> Response:
            try:
                decoded_auth_info = self.check_auth_headers(request)
            except NotAuthorized as error:
                content = {'detail': error.detail}
                return JSONResponse(status_code=error.status_code, content=content)

            request.state.user_info = {}
            if decoded_auth_info:
                request.state.user_info = {
                    'id': decoded_auth_info['id']
                }

            return await original_route_handler(request)
        return custom_route_handler

    @classmethod
    def check_auth_headers(cls, request: Request) -> dict | None:
        if not (access_token := request.headers.get('Authorization')):
            return None

        decoded = decode_token(access_token[7:])
        if decoded:
            return decoded
        raise NotAuthorized()
