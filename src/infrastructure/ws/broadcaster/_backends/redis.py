import asyncio
import typing

import aioredis

from .._base import Event
from .base import BroadcastBackend


class RedisBackend(BroadcastBackend):
    def __init__(self, url: str):
        self._url = url

    async def connect(self) -> None:

        try:
            self._pub_conn = aioredis.Redis.from_url(
                self._url, max_connections=2, decode_responses=True
            )
            self._sub_conn = aioredis.Redis.from_url(
                self._url, max_connections=2, decode_responses=True
            )
            self._subscriber = self._sub_conn.pubsub()
            await self._subscriber.subscribe('default')
        except Exception as e:
            print(e)

    async def disconnect(self) -> None:
        await self._subscriber.close()
        await self._pub_conn.close()
        await self._sub_conn.close()

    async def reconnect(self):
        try:
            channels = self._subscriber.channels
            await self.connect()
            for channel in channels.keys():
                await self._subscriber.subscribe(channel)
        except Exception as E:
            import traceback
            traceback.print_exc()

    def health_check(self):
        return self._subscriber.connection.is_connected

    async def subscribe(self, channel: str) -> None:
        await self._subscriber.subscribe(channel)

    async def unsubscribe(self, channel: str) -> None:
        await self._subscriber.unsubscribe(channel)

    async def publish(self, channel: str, message: typing.Any) -> None:
        await self._pub_conn.publish(channel, message)

    async def next_published(self) -> Event:
        while 'ебучий редис запущен':
            if not self._subscriber.subscribed:
                await asyncio.sleep(0.01)
                continue

            message = await self._subscriber.get_message(ignore_subscribe_messages=True)
            if message:
                return Event(channel=message['channel'], message=message['data'])
            await asyncio.sleep(0)
