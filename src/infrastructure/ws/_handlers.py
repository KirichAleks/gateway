from src.domain.chat import handlers as chat_handlers


handlers_mapper = {
    'chat': chat_handlers
}
