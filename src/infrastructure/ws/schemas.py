from typing import Any
from pydantic import BaseModel


class WSRequest(BaseModel):
    message_type: str
    payload: dict
    request_id: str | None = None


class WSResponse(BaseModel):
    message_type: str
    payload: Any
    request_id: str | None
