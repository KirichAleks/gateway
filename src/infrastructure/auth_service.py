from authlib.jose import jwt
from authlib.jose.errors import JoseError

from src.infrastructure.types import StrEnum
from src.settings import get_settings

settings = get_settings()


class Tokens(StrEnum):
    ACCESS = 'access'
    REFRESH = 'refresh'
    WS_TOKEN = 'ws_token'


tokens_info = {
    Tokens.REFRESH: settings.jwt_refresh_secret,
    Tokens.ACCESS: settings.jwt_access_secret,
    Tokens.WS_TOKEN: settings.jwt_ws_secret,
}


def decode_token(token: str, token_type: Tokens = Tokens.ACCESS) -> dict | None:
    try:
        return jwt.decode(token, tokens_info[token_type])
    except JoseError:
        return None
