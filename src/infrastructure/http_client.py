import httpx

from src.domain.enums import Services
from src.infrastructure.exceptions import InfrastructureException
from src.settings import get_settings

settings = get_settings()


domains = {
    Services.USERS: settings.domains.users,
    Services.CHAT: settings.domains.chat
}


class HttpClient:
    @classmethod
    async def send_request(
            cls,
            method: str,
            url: str,
            data: dict | None = None,
            params: dict | None = None,
            headers: dict | None = None,
            *,
            service: Services | None,
            request_user: str | None = None,
    ):
        headers = headers or {}
        if request_user:
            headers['x-user-id'] = request_user
        # if request_id:
        #     headers['x-request-id'] = request_id

        _kwargs = {
            'params': params,
            'headers': headers,
        }
        if method.lower() != 'get':
            _kwargs['json'] = data
        full_url = f'{domains[service]}{url}'
        print(full_url)
        async with httpx.AsyncClient() as client:
            request = getattr(client, method.lower())
            r = await request(
                full_url,
                **_kwargs,
                timeout=10.0
            )

            if 200 <= (status_code := r.status_code) < 300:
                return r.json()
            elif status_code < 400:
                """redirect"""
            else:
                raise InfrastructureException(status_code)

        return None
