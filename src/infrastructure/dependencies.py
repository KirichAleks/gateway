from dataclasses import dataclass

from fastapi import Request


@dataclass
class UserInfo:
    id: str


def get_request_user(request: Request) -> UserInfo:
    return UserInfo(**request.state.user_info)
